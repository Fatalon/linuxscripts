read -p "- Backlight settings [scale from 1 to 10] ? "

if [[ "$REPLY" =~ ^[0-9]+$ ]]; then
if [ "$REPLY" -ge 1 -a "$REPLY" -le 10 ]; then scale="$REPLY";fi
else echo "$REPLY is not an integer" >&2 && exit 1; fi

echo "Set scale to : $scale"
