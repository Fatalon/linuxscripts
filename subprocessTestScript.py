#!/usr/bin/python
# prints ls -l /tmp/
# subprocess.call(["some_command", "some_argument", "another_argument_or_path"])

# or subprocess.call("ls -l", shell=True)
import subprocess

subprocess.call(["ls", "-l", "/tmp/"])

subprocess.call(["vlc", "-LZ", "home/paul/Music"])

# General
# make a python Script executable by running chmod +x pyls.py
