#!/usr/bin/python
# Python3 Script for playing music


# check for internet connection
import requests


def check_InternetConnection():
    url = 'http://www.google.com/'
    timeout = 5
    try:
        _ = requests.get(url, timeout=timeout)
        return True
    except requests.ConnectionError:
        print("An Internet connection is required!")
    return False


def validateThatMethodWorksAsExpected():
    result = check_InternetConnection()
    print(result)
