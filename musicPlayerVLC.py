#!/usr/bin/python
# find ~/Music/*/./* -type f -name "*.mp3" -exec vlc --one-instance --playlist-enqueue --playlist-autostart --fullscreen -Z '{}' +

import subprocess
import psutil

def kill_process_if_already_running():
        for proc in psutil.process_iter(attrs=['pid', 'name']):
                if proc.info['name'] == "vlc":
                        #Process is running
                        proc.kill()
                        return True
        else:
                return False


if not kill_process_if_already_running():
        subprocess.call("find ~/Music/*/./* -type f -name \"*.mp3\" -exec cvlc --one-instance --playlist-enqueue --playlist-autostart --fullscreen -Z '{}' +",shell=True)

